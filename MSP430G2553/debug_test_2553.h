/*
 * debug_test_2553.h
 *
 *  Created on: 29 mars 2021
 *      Author: mourad - shesley
 */

#ifndef DEBUG_H_
#define DEBUG_H_

#include <msp430g2553.h>
#include <def_2553.h>
#include <uart.h>
#include <motors.h>

void test_echo_UART(BOOLEAN test);
void test_LED(BOOLEAN test);
void test_toggle_LED(unsigned int i);

#endif
