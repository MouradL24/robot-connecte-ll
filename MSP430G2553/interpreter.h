/*
 * interpreter.h
 *
 *  Created on: 11 mars 2021
 *      Author: mourad - shesley
 */

#ifndef INTERPRETER_H_
#define INTERPRETER_H_

#include <msp430g2553.h>
#include <def_2553.h>
#include <motors.h>
#include <ressources.h>
#include <uart.h>
#include<string.h>


void interpreter_UART(void);
void interpreter_SPI(void);

#endif /* INTERPRETER_H_ */
