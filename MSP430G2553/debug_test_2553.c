/*
 * debug_test_2553.c
 *
 *  Created on: 29 mars 2021
 *      Author: mourad - shesley
 */

#include <debug_test_2553.h>

void test_echo_UART(BOOLEAN test){

    if(test){
        send_msg_UART("TRUE");
    }
    else{
        send_msg_UART("FALSE");
    }

}

void test_LED(BOOLEAN test){
    if(test){
       P1OUT |= LED;
    }
    else{
       P1OUT &= ~LED;
    }
}

void test_toggle_LED(unsigned int d){

 unsigned int i;
   for(i=0;i<d;i++){
   P1OUT |=BIT6;
   delay(d);
   P1OUT &= ~(BIT6);
   delay(d);
   }

}
