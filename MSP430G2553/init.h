/*
 * init.h
 *
 *  Created on: 11 mars 2021
 *      Author: mourad - shesley
 */

#ifndef INIT_H_
#define INIT_H_

#include <msp430g2553.h>
#include <def_2553.h>


void InitBOARD(void);
void InitUART(void);
void InitSPI(void);


#endif /* INIT_H_ */
