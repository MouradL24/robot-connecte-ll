/*
 * servomotors.h
 *
 *  Created on: 11 mars 2021
 *  Author: mourad - shesley
 */

#ifndef MOTORS_H_
#define MOTORS_H_

#include <msp430g2553.h>
#include <def_2553.h>


void forward( void );
void left( void );
void right( void );
void back( void );
void stop( void );
void left_rotation( void );
void right_rotation( void );
void delay(unsigned long time);
void move_forward(unsigned long speed, unsigned long time);
void move_back(unsigned long speed, unsigned long time);
void move_left_rotation(unsigned long speed, unsigned long time);
void move_right_rotation(unsigned long speed, unsigned long time);



#endif /* MOTORS_H_ */
