/*
 * ressources.h
 *
 *  Created on: 11 mars 2021
 *      Author: mourad - shesley
 */

#ifndef RESSOURCES_H_
#define RESSOURCES_H_

#include <msp430g2553.h>
#include <def_2553.h>


extern COMMAND command;
extern COMMAND command_2231;
extern MODE mode;
extern BOOLEAN running;
extern BOOLEAN autorun;
extern BOOLEAN int_UART;
extern BOOLEAN int_SPI;
extern ANGLE angle_servo;


extern unsigned int num_car;
extern unsigned char cmd[CMDLEN_MAX];

extern unsigned char distance_char;
extern unsigned char* distance_obs;

extern unsigned char distance_obs_ang[5];



#endif /* RESSOURCES_H_ */
