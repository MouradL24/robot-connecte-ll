/*
 * init.c
 *
 *  Created on: 11 mars 2021
 *      Author: mourad - shesley
 */



#include <init.h>

void InitBOARD(void)
{
    WDTCTL = WDTPW + WDTHOLD;   /* Stop WDT */
   /* clock calibration verification */
   if((CALBC1_1MHZ==0xFF) || (CALDCO_1MHZ==0xFF))
   {
     __low_power_mode_4();
   }
   /* factory calibration parameters */
   DCOCTL = 0;
   BCSCTL1 = CALBC1_1MHZ;
   DCOCTL = CALDCO_1MHZ;

   P2DIR |=(BIT_DIR_A|BIT_DIR_B|BIT_PWM_A|BIT_PWM_B);
   P2SEL |= (BIT_PWM_A|BIT_PWM_B);
   P2SEL2 &=~(BIT_PWM_A|BIT_PWM_B);
   P2OUT &=~BIT_DIR_A;
   P2OUT |=(BIT_DIR_B);

   TA1CTL|= (TASSEL_2 + MC_1 + ID_0);
   TA1CCTL1 |= OUTMOD_7;
   TA1CCTL2 |= OUTMOD_7;
   TA1CCR0=PERIOD_PWM;

   }

void InitUART(void)
{
    P1SEL |= (TXD | RXD);
    P1SEL2 |= (TXD | RXD);
    UCA0CTL1 |= UCSWRST;
    UCA0CTL1 |= UCSSEL_2;                   /* SMCLK */
    UCA0BR0 = 104;                          /* 1MHz, 9600 baud */
    UCA0BR1 = 0;                            /* 1MHz, 9600 baud */
    UCA0MCTL = 10;
    UCA0CTL0 &= ~UCPEN & ~UCPAR & ~UCMSB;
    UCA0CTL0 &= ~UC7BIT & ~UCSPB & ~UCMODE1;
    UCA0CTL0 &= ~UCMODE0 & ~UCSYNC;
    UCA0CTL1 &= ~UCSWRST;

    /* Enable USCI_A0 RX interrupt */
    IE2 |= UCA0RXIE;
}

void InitSPI(void){
        __delay_cycles(250);


                  UCB0CTL0 = 0;
                 UCB0CTL1 = (0 + (UCSWRST*1) );
                 IFG2 &= ~(UCB0TXIFG | UCB0RXIFG);

                  P1OUT |= CS;
                  P1DIR |= CS;
                  P1SEL |= (SCK | DATA_IN | DATA_OUT);
                  P1SEL2 |= (SCK | DATA_IN | DATA_OUT);
                  UCB0CTL0 |= ( UCMST | UCMODE_0 | UCSYNC );
                  UCB0CTL0 &= ~( UCCKPH | UCCKPL | UCMSB | UC7BIT );
                  UCB0CTL1 |= UCSSEL_2;
                  UCB0BR0 = 0x0A;
                  UCB0BR1 = 0x00;

                  UCB0CTL1 &= ~UCSWRST;  /* **Initialize USCI state machine** */

}



