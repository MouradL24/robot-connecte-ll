#ifndef typedef_H
#define typedef_H



typedef char						CHAR;
typedef unsigned char					UCHAR;
typedef unsigned short					UINT_16;
typedef unsigned int					UINT_32;
typedef signed char					SCHAR;
typedef signed short					SINT_16;
typedef signed int					SINT_32;
typedef float						FLOAT_32;
typedef double						FLOAT_64;
typedef unsigned long					ULONG;
typedef enum { FALSE = 0, TRUE  = 1}			BOOL;

#endif