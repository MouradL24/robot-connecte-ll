#include <msp430.h> 
#include <intrinsics.h>
#include <ADC.h>
#include <servo.h>
#include <IR.h>
#include <typedef.h>




volatile UCHAR RXDta;
volatile UCHAR TXDta = 'n';
UINT_32 distance=0U;
UCHAR distance_char='n';
SINT_32 angle = 90;

UCHAR voie=3;



void init_BOARD(void);
void init_SPI(void);
UCHAR get_distance_char(UINT_32 dist);

SINT_32 main(void)
{
    init_BOARD();
    init_SPI();
    init_servo();
    ADC_init();

    while ((P1IN & BIT5)!=0)
	{
	}
	while(TRUE)
	{
	    while( (USICTL1 & USIIFG)==0 )
		{
		}	
	     RXDta = USISRL;

	      switch(RXDta){
	          case 'w':
	              distance = distance_measure(voie);
                  distance_char=get_distance_char(distance);
                  TXDta=distance_char;
	          break;
	          case 'v':
	              angle=180;
                  (void)servo_set(angle);
               break;
	           case 'x':
	               angle=0;
                  (void)servo_set(angle);
               break;
              case 'g':
                  if(angle<135){
                      angle+=45;
                  }
                  else{
                      angle=180;
                  }
                  (void)servo_set(angle);
               break;
              case 'f':
                  if(angle>=45){
                       angle-=45;
                  }
                  else{
                      angle=0;
                  }
                  (void)servo_set(angle);
               break;
              case 'c':
                    angle=90;
                    (void)servo_set(angle);
                 break;
	          default :
			/*  */
	          break;
	          }

            USISRL = TXDta;
            USICNT &= ~USI16B;
            USICNT = 0x08;

	}
	return 0;
}


UCHAR get_distance_char(UINT_32 dist){

   UCHAR dis_char='n';

   switch(dist){

   case 36U:
       dis_char='9';
       break;
   case 32U:
       dis_char='8';
       break;
   case 28U:
       dis_char='7';
       break;
   case 24U:
       dis_char='6';
       break;
   case 20U:
       dis_char='5';
       break;
   case 16U:
       dis_char='4';
       break;
   case 12U:
       dis_char='3';
       break;
   case 8U:
       dis_char='2';
       break;
   case 4U:
       dis_char='1';
       break;
   case 0U:
       dis_char='0';
       break;
   default:
       dis_char='n';
       break;
   }

   return dis_char;
}

void init_BOARD(void){
    WDTCTL = WDTPW | WDTHOLD;   /* stop watchdog timer*/
    BCSCTL1= CALBC1_1MHZ; /*frequence d�horloge 1MHz*/
    DCOCTL= CALDCO_1MHZ;

        P1SEL &= ~(BIT3|BIT0);
        P1DIR &= ~(BIT3);
        P1DIR |=BIT0;
        P1OUT &= ~(BIT0);


}

void init_SPI(void){
    USICTL0 |= USISWRST;
    USICTL1 = 0;
    USICTL0 |= (USIPE7 | USIPE6 | USIPE5 | USILSB | USIOE | USIGE );
    USICTL0 &= ~(USIMST);
    USICTL1 |= USIIE;
    USICTL1 &= ~(USICKPH | USII2C);
    USICKCTL = 0;
    USICKCTL &= ~(USICKPL | USISWCLK);
    USICNT = 0;
    USICNT &= ~(USI16B | USIIFGCC );
    USISRL = 0x23;
    USICNT = 0x08;
    USICTL0 &= ~USISWRST;
}


